# devsrvr

devsrvr is a simple tool to ease local web development in the [Go](http://golang.org/) programming language.
Its main purpose in life is to sit in front of your webapp and automatically recompile it as needed when
new requests come in, making the edit/reload cycle much quicker and more productive.

## Usage

To use devsrvr, you need to adjust your app to accept a flag called `addr`, which it uses to configure the address it listens on. A simple example looks like:

    :::go
    import (
	    "flag"
        "net/http"
    )

    var addr = flag.String("addr", ":9090", "address")

    func main() {
        flag.Parse()
        log.Println("listening on:", *addr)
        log.Fatal(http.ListenAndServe(*addr, http.HandlerFunc(fooHandler)))
    }

By default, devsrvr will listen on `localhost:8000`, and instruct your app to listen on `localhost:8888`.

So, to see your app you'd run

    devsrvr /path/to/my/app

and visit `localhost:8000` in your browser.

Each time you refresh the page, devsrvr will check whether your app needs to be rebuilt, and handle it if necessary. Any build errors are returned to you in the browser.

### Options

By default, devsrvr waits one second for your app to start up before proxying the first request, in order to catch any errors that occur on startup.

You can adjust this startup timeout as well with the `startup` flag:

    devsrvr -startup 0.5s /path/to/my/app

## License

devsrvr is available under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).
